package utils;

import libs.Budget;

/**
 * Created by Andrew on 2019-10-09.
 */
public class Utilities {

    //Assume the year to be the latest possible year, based on the budget end date.
    public static int assumeSuitableYear(int month, int day)
    {
        int year = Budget.getTimeframe().getToDate().getYear(); //Attempt to use the final budget year
        int startYear = Budget.getTimeframe().getFromDate().getYear();
        int endMonth = Budget.getTimeframe().getToDate().getMonthValue();
        int endDay = Budget.getTimeframe().getToDate().getDayOfMonth();

        //Check if need to use the year before the final budget year.
        if (endMonth < month) { //If the budget end month is earlier than the given month...
            //Ensure the year before the final budget year is not before the start year.
            year = Math.max(startYear, year - 1);
        }
        else if (endMonth == month)
        {
            if (endDay < day) //If budget end day is earlier than the given day...
            {
                year = Math.max(startYear, year - 1);
            }
        }
        return year;
    }

    //Returns the amount of days in the given month.
    //Returned month starts with index at 0.
    public static int getLastDayOfMonth(int month)
    {
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                return 28;
            default:
                return 31;
        }
    }

    public static int monthToInt(String month)
    {
        switch (month)
        {
            case "Jan":
                return 1;
            case "Feb":
                return 2;
            case "Mar":
                return 3;
            case "Apr":
                return 4;
            case "May":
                return 5;
            case "Jun":
                return 6;
            case "Jul":
                return 7;
            case "Aug":
                return 8;
            case "Sep":
                return 9;
            case "Oct":
                return 10;
            case "Nov":
                return 11;
            case "Dec":
                return 12;
            default:
                return 1;
        }
    }
}
