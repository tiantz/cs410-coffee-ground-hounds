package utils;

import java.util.Arrays;
import java.util.List;

public class Consts {
    public static final String MAKEABUDGET = "Make a budget";
    public static final String CALLED = "called";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String DISPLAYAS = "Display as";
    public static final String SPEND = "Spend";
    public static final String CURRENTBAL = "Current balance is";
    public static final String EARN = "Earn";
    public static final String SAVE = "Save";
    public static final String BY = "by";
    public static final String EVERY = "every";
    public static final String FOR_NOSPACE = "for";
    public static final String FOR_WITHSPACE = " for ";  // small hacky fix for tokenizing
    public static final String NEXT = "next";
    public static final String ONLYSHOW_NOSPACE = "only show";
    public static final String ONLYSHOW_WITHSPACE = "only show ";

    public static final List<String> ALL_LITERALS =
            Arrays.asList(MAKEABUDGET, CALLED, FROM, TO, DISPLAYAS, SPEND,
                    CURRENTBAL, EARN, SAVE, BY, EVERY,
                    FOR_WITHSPACE, NEXT, ONLYSHOW_WITHSPACE);

    public static final String YEAR = "year";
    public static final String MONTH = "month";
    public static final String WEEK = "week";
    public static final String DAY = "day";
    public static final List<String> PERIODTYPES = Arrays.asList(YEAR, MONTH, WEEK, DAY);

    public static final String PIE = "pie";
    public static final String TABLE = "table";
    public static final String BAR = "bar";
    public static final List<String> DISPLAYTYPES = Arrays.asList(PIE, TABLE, BAR);

}
