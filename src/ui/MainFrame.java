package ui;

import ast.BALANCE;
import ast.PROGRAM;
import libs.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import utils.Consts;
import org.jfree.chart.*;
import org.jfree.chart.plot.Plot;
import org.jfree.data.general.DefaultPieDataset;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

public class MainFrame extends JFrame {

    private static final int MAIN_GUI_WIDTH = 800;
    private static final int MAIN_GUI_HEIGHT = 800;

    private String inputFilename = "input.txt";
    private String outputFilename = "output.txt";
    private Budget budget = Budget.getInstance();
    private Budget.DisplayMode displayMode = budget.getDisplayMode();
    private String totalPeriodType = Budget.getPeriodType();
    private Integer totalPeriodLength = Budget.getPeriodLength();

    private List<Income> incomes = new ArrayList();
    private List<Spending> spendings = new ArrayList();
    private List<Saving> savings = new ArrayList();

    private FlowLayout contentPanelLayout = new FlowLayout();
    private JPanel configPanel = new JPanel();
    private JPanel contentPanel = new JPanel();
    private  JScrollPane scrolframe;
    private JTextArea textAreaUserInput = null;

    public MainFrame() {

        // Set content panel properties
        contentPanel.setLayout(contentPanelLayout);
        contentPanelLayout.setAlignment(FlowLayout.CENTER);
        contentPanel.setPreferredSize(new Dimension(500, 950));
        contentPanel.setMinimumSize(new Dimension(500, 950));

        // Set control panel properties
        configPanel.setLayout(new FlowLayout());
//        configPanel.setPreferredSize(new Dimension(400, 150));
//        configPanel.setMinimumSize(new Dimension(400, 100));

        // Text area to save user input
        try {
            // Initially copy content from input.txt to text area.
//            textAreaUserInput = new JTextArea(Files.readString(Paths.get("..\\"+inputFilename)));
            textAreaUserInput = new JTextArea(new String(Files.readAllBytes(Paths.get(inputFilename)), StandardCharsets.UTF_8));

        } catch (IOException e) {
            System.out.println("Didn't find file 2");
            System.exit(0);
        }

        // Set text area properties
        textAreaUserInput.setPreferredSize(new Dimension(400, 200));
        textAreaUserInput.setMinimumSize(new Dimension(200, 150));
        textAreaUserInput.setBorder(BorderFactory.createLineBorder(Color.gray));
        textAreaUserInput.setLineWrap(true);
        textAreaUserInput.setWrapStyleWord(true);
        configPanel.add(textAreaUserInput);

        // Set up button box holding buttons
        Box buttonBox = Box.createVerticalBox();

        // Button to press to run the user code
        JButton btnRun = new JButton("Run");
        btnRun.addActionListener(new ActionListener() { // When user clicks RUN button
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                runUserCode();
            }
        });
        buttonBox.add(btnRun);

        // Button to press to clear the user code
        JButton btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                textAreaUserInput.setText("");
            }
        });
        buttonBox.add(btnClear);
        configPanel.add(buttonBox);

        // Main body
        setLayout(new BorderLayout());
        scrolframe = new JScrollPane(contentPanel);
//        add("North", contentPanel);
        contentPanel.setAutoscrolls(true);
        scrolframe.setPreferredSize(new Dimension(800, 550));
        this.add("North", scrolframe);
        add("South",configPanel);
        pack();
        setTitle("Budget Plan Generator");
        setSize(MAIN_GUI_WIDTH,MAIN_GUI_HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
    private void runUserCode() {
        // overwrite input.txt with user input from component textAreaUserInput
        overwriteInputFile();

        Tokenizer.makeTokenizer(inputFilename,Consts.ALL_LITERALS);
        Tokenizer.resetTokenizer();
        System.out.println("Done tokenizing");
        try {
            Node.setWriter("output.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Node program = new PROGRAM();
        program.parse();
        System.out.println("Done parsing");
        program.namecheck();
        program.typecheck();
        System.out.println("Done type checking");
        Budget.refreshBudget();
        program.evaluate();
        System.out.println("Done evaluation");
        Node.closeWriter();

        // update variables here
        incomes = Budget.getIncomes();
        spendings = Budget.getSpendings();
        savings = Budget.getSavings();
        displayMode = Budget.getDisplayMode();
        totalPeriodType = Budget.getPeriodType();
        totalPeriodLength = Budget.getPeriodLength();

        // filter using select
        if (Budget.getSelectInfo() != null) {
            applySelect();
        }

        // Display data conetent
        displayDataContent();
        System.out.println("Done displaying info");
    }

    private void applySelect() {
        Select select = Budget.getSelectInfo();
        if (!select.getCategory().equals("")) {
            String selectCategory = select.getCategory();

            List<Income> selected_income = new ArrayList<>();
            for (Income income: incomes) {
                if (income.getCategory().equals(selectCategory)) {
                    selected_income.add(income);
                }
            }
            incomes = selected_income;

            List<Spending> selected_spending = new ArrayList<>();
            for (Spending spending: spendings) {
                if (spending.getCategory().equals(selectCategory)) {
                    selected_spending.add(spending);
                }
            }
            spendings = selected_spending;

            List<Saving> selected_savings = new ArrayList<>();
            for (Saving saving: savings) {
                if (saving.getPurpose().equals(selectCategory)) {
                    selected_savings.add(saving);
                }
            }
            savings = selected_savings;
        }

        Timeframe tf = select.getTimeframe();
        totalPeriodLength = tf.getDays();
        totalPeriodType = Consts.DAY;
    }

    private void overwriteInputFile() {
        // overwrite current user code to file: input.txt
        try {
//            FileWriter f = new FileWriter(new File(inputFilename), false);
//            f.write(userCode);
//            f.close();
            String userCode = textAreaUserInput.getText();
            RandomAccessFile overwrittenInputFile = new RandomAccessFile("input.txt", "rw");
            overwrittenInputFile.writeUTF(userCode);
            overwrittenInputFile.seek(0);
            overwrittenInputFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void displayDataContent() {
        // Determine and display data
        if (displayMode == Budget.DisplayMode.PIE) {
            displayAsPie();
        } else if (displayMode == Budget.DisplayMode.TABLE) {
            displayAsTable();
        } else if (displayMode == Budget.DisplayMode.BAR) {
            disPlayAsBar();
        }
    }

    private void displayAsPie() {
        DefaultPieDataset incomeDataset = new DefaultPieDataset();

        for (int i = 0; i < incomes.size(); i++) {
            Income income = incomes.get(i);
            String category = income.getCategory();
            Float amount = income.getAmountTotal(totalPeriodLength, totalPeriodType);
            incomeDataset.setValue(category, amount);
        }

        JFreeChart incomeChart = ChartFactory.createPieChart("INCOME", incomeDataset, true, true, true);

        DefaultPieDataset spendingDataset = new DefaultPieDataset();
        for (int i = 0; i < spendings.size(); i++) {
            Spending spending = spendings.get(i);
            String category = spending.getCategory();
            Float amount = spending.getAmountTotal(totalPeriodLength, totalPeriodType);
            spendingDataset.setValue(category, amount);
        }
        JFreeChart spendingChart = ChartFactory.createPieChart("SPENDING", spendingDataset, true, true, true);

        DefaultPieDataset savingDataset = new DefaultPieDataset();
        for (int i = 0; i < savings.size(); i++) {
            Saving saving = savings.get(i);
            String category = saving.getPurpose();
            Float amount = saving.getAmount();
            savingDataset.setValue(category, amount);
        }
        JFreeChart savingChart = ChartFactory.createPieChart("Saving", spendingDataset, true, true, true);


        Plot incomeCp = incomeChart.getPlot();
        Plot spendingCp = spendingChart.getPlot();
        Plot savingCp = savingChart.getPlot();

        incomeCp.setBackgroundPaint(ChartColor.white);
        spendingCp.setBackgroundPaint(ChartColor.white);
        savingCp.setBackgroundPaint(ChartColor.white);

//        try {
//            ChartUtilities.saveChartAsPNG(new File("imgs/PieChart_income.png"), chart, 400, 400);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        ChartPanel incomeChartPanel = new ChartPanel(incomeChart);
        ChartPanel spendingChartPanel = new ChartPanel(spendingChart);
        ChartPanel savingChartPanel = new ChartPanel(savingChart);
        contentPanel.removeAll();
        contentPanel.add(incomeChartPanel);
        contentPanel.add(spendingChartPanel);
        contentPanel.add(savingChartPanel);
        contentPanel.setBorder(BorderFactory.createLineBorder(Color.gray));
        contentPanel.updateUI();
    }

    private void displayAsTable() {
        String[] incomecolumnNames = {"Income Categories", "Value"};
        String[] savingscolumnNames = {"Savings Categories", "Value"};
        String[] spendingcolumnNames = {"Spending Categories", "Value"};
        String[] summarycolumnNames = {"Summarized Categories", "Value"};
        float totalSavings = 0;
        float totalSpendings = 0;
        float totalIncome = 0;
        float preBalance = Budget.getBalance();
        float postBalance = preBalance;

        // INCOME
        Object[][] incomesDataSet = new Object[incomes.size()][incomecolumnNames.length];

        for (int i = 0; i < incomes.size(); i++) {
            Income income = incomes.get(i);
            String category = income.getCategory();
            Float amount = income.getAmountTotal(totalPeriodLength, totalPeriodType);
            incomesDataSet[i][0] = category;
            incomesDataSet[i][1] = amount;
            totalIncome += amount;
            postBalance += amount;
        }

        JTable incomeTable = new JTable(incomesDataSet, incomecolumnNames);

        incomeTable.setBorder(BorderFactory.createLineBorder(Color.black));
        incomeTable.setGridColor(Color.black);
        incomeTable.setPreferredScrollableViewportSize(new Dimension(400, 70));
        incomeTable.setFillsViewportHeight(true);

        // SPENDING
        Object[][] spendingDataSet = new Object[spendings.size()][spendingcolumnNames.length];

        for (int i = 0; i < spendings.size(); i++) {
            Spending spending = spendings.get(i);
            String category = spending.getCategory();
            Float amount = spending.getAmountTotal(totalPeriodLength, totalPeriodType);
            spendingDataSet[i][0] = category;
            spendingDataSet[i][1] = amount;
            totalSpendings += amount;
            postBalance -= amount;
        }

        JTable spendingTable = new JTable(spendingDataSet, spendingcolumnNames);

        spendingTable.setBorder(BorderFactory.createLineBorder(Color.black));
        spendingTable.setGridColor(Color.black);
        spendingTable.setPreferredScrollableViewportSize(new Dimension(400, 70));
        spendingTable.setFillsViewportHeight(true);

        // SAVINGS
        Object[][] savingsDataSet = new Object[savings.size()][savingscolumnNames.length];

        for (int i = 0; i < savings.size(); i++) {
            Saving saving = savings.get(i);
            String category = saving.getPurpose();
            Float amount = saving.getAmount();
            savingsDataSet[i][0] = category;
            savingsDataSet[i][1] = amount;
            totalSavings += amount;
            postBalance -= amount;
        }

        JTable savingsTable = new JTable(savingsDataSet, savingscolumnNames);
        
        savingsTable.setBorder(BorderFactory.createLineBorder(Color.black));
        savingsTable.setGridColor(Color.black);
        savingsTable.setPreferredScrollableViewportSize(new Dimension(400, 70));
        savingsTable.setFillsViewportHeight(true);

        // TOTAL Balance
        Object[] category = {"Pre Balance", "Total Income", "Total Spending", "Total Saving", "Post Balance"};
        Object[] amount = {preBalance,totalIncome,totalSpendings,totalSavings, postBalance};

        Object[][] summaryDataSet = new Object[amount.length][summarycolumnNames.length];

        for (int i = 0; i < category.length; i++) {
            summaryDataSet[i][0] = category[i];
            summaryDataSet[i][1] = amount[i];
        }

        JTable summaryTable = new JTable(summaryDataSet, summarycolumnNames);

        summaryTable.setBorder(BorderFactory.createLineBorder(Color.black));
        summaryTable.setGridColor(Color.black);
        summaryTable.setPreferredScrollableViewportSize(new Dimension(400, 80));
        summaryTable.setFillsViewportHeight(true);


        JScrollPane incomeScrollPane = new JScrollPane(incomeTable);
        JScrollPane spendingScrollPane = new JScrollPane(spendingTable);
        JScrollPane savingsScrollPane = new JScrollPane(savingsTable);
        JScrollPane summaryScrollPane = new JScrollPane(summaryTable);
        contentPanel.removeAll();
        contentPanel.add(spendingScrollPane);
        contentPanel.add(incomeScrollPane);
        contentPanel.add(savingsScrollPane);
        contentPanel.add(summaryScrollPane);
        contentPanel.setBorder(BorderFactory.createLineBorder(Color.gray));
        contentPanel.updateUI();
    }

    private void disPlayAsBar() {
        // income bar chart
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
        for(int i=0 ;i<incomes.size(); i++){
            Income icm = incomes.get(i);
            String category = icm.getCategory();
            String period = "every" +icm.getPeriodLength()+ " " + icm.getPeriodType();
            Float amt = icm.getAmountTotal(totalPeriodLength, totalPeriodType);

            dataset.addValue(amt, category, ""+i);
        }
        JFreeChart incomeBar = ChartFactory.createBarChart("INCOME", "Category",
                "Amount", dataset, PlotOrientation.VERTICAL, true, true, false);
        //display bar chart
        BarRenderer br = (BarRenderer) incomeBar.getCategoryPlot().getRenderer();
        br.setMaximumBarWidth(.35);
        br.setItemMargin(.1);
        Plot incomeCp = incomeBar.getPlot();
        incomeCp.setBackgroundPaint(ChartColor.white);
        ChartPanel incomeChartPanel = new ChartPanel(incomeBar);
        incomeChartPanel.setPreferredSize(new java.awt.Dimension(400, 300));
        contentPanel.add(incomeChartPanel);


        // Spending chart
        DefaultCategoryDataset spendingDataset = new DefaultCategoryDataset( );
        for(int i=0 ;i<spendings.size(); i++){
            Spending spd = spendings.get(i);
            String category = spd.getCategory();
            String period = "every" +spd.getPeriodLength()+ " " + spd.getPeriodType();
            Float amt = spd.getAmountTotal(totalPeriodLength, totalPeriodType);

            spendingDataset.addValue(amt, category, ""+i);
        }
        JFreeChart spendingBar = ChartFactory.createBarChart("SPENDING", "Category",
                "Amount", spendingDataset, PlotOrientation.VERTICAL, true, true, false);
        //spending display
        BarRenderer spdbr = (BarRenderer) spendingBar.getCategoryPlot().getRenderer();
        spdbr.setMaximumBarWidth(.35);
        spdbr.setItemMargin(.1);
        Plot spendCp = spendingBar.getPlot();
        spendCp.setBackgroundPaint(ChartColor.white);
        ChartPanel spendChartPanel = new ChartPanel(spendingBar);
        spendChartPanel.setPreferredSize(new java.awt.Dimension(400, 300));
        contentPanel.add(spendChartPanel);

        //Savings
        DefaultCategoryDataset saveDateset = new DefaultCategoryDataset();
        for(int i=0; i<savings.size(); i++){
            Saving save = savings.get(i);
            String purpose = save.getPurpose();
            Float amt = save.getAmount();
            saveDateset.addValue(amt, purpose, ""+i);
        }
        JFreeChart saveChart = ChartFactory.createBarChart("SAVINGS", "Purpose",
         "Amount", saveDateset, PlotOrientation.VERTICAL, true, true, false);
        BarRenderer saveBr = (BarRenderer) saveChart.getCategoryPlot().getRenderer();
        saveBr.setMaximumBarWidth(.35);
        br.setItemMargin(.1);
        Plot saveCp = saveChart.getPlot();
        saveCp.setBackgroundPaint(ChartColor.white);
        ChartPanel saveChartPanel = new ChartPanel(saveChart);
        saveChartPanel.setPreferredSize(new java.awt.Dimension(400, 300));
        contentPanel.add(saveChartPanel);


        contentPanel.setBorder(BorderFactory.createLineBorder(Color.gray));

    }

}