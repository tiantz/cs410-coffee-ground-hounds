package ast;

import libs.Budget;
import libs.Node;
import libs.Timeframe;
import utils.Consts;
import utils.Utilities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PROGRAM extends Node {

    private PERIOD period;
    private DATE from_date;
    private List<STATEMENT> statements = new ArrayList<>();
    private DISPLAYMODE displaymode;

    String title;

    @Override
    public void parse() {
        // Make a budget , for,  next
        tokenizer.getAndCheckNext(Consts.MAKEABUDGET);
        tokenizer.getAndCheckNext(Consts.FOR_NOSPACE);
        tokenizer.getAndCheckNext(Consts.NEXT);

        // 3 months
        period = new PERIOD();
        period.parse();
        // from , Sep 1
        tokenizer.getAndCheckNext(Consts.FROM);
        from_date = new DATE();
        from_date.parse();

        // called , Charlie’s budget
        if (tokenizer.checkToken(Consts.CALLED)) {
            tokenizer.getAndCheckNext(Consts.CALLED);
            title = tokenizer.getNext();
            System.out.println(title);
        } else {
            title = "Budget Report";
        }

        // Current balance is, 5, [ETC]
        while (!tokenizer.checkToken(Consts.DISPLAYAS) &&
               tokenizer.moreTokens()) {
            STATEMENT s = new STATEMENT();
            s.parse();
            statements.add(s);
        }

        // Display as, table
        displaymode = new DISPLAYMODE();
        if (tokenizer.checkToken(Consts.DISPLAYAS)) {
            displaymode.parse();
        } // Default display mode is table, display everything
    }

    @Override
    public void evaluate() {
        //TODO: Refactor this into helper methods.
        int startYear = LocalDate.now().getYear();
        int startMonth = Utilities.monthToInt(from_date.month);
        int startDay = Integer.parseInt(from_date.day);
        LocalDate startTime = LocalDate.of(startYear, startMonth, startDay);

        Budget.setPeriodLength(Integer.parseInt(period.length));
        Budget.setPeriodType((period.type));

        int endYear = startYear;
        int endMonth = startMonth;
        int endDay = startDay;
        int currentMonth = endMonth;

        //Calculate the end date of the budget:
        //Append period to start dates.
        switch(period.type)
        {
            case "days":
                endDay = startDay + Budget.getPeriodLength();
                break;
            case "weeks":
                endDay = startDay + 7 * Budget.getPeriodLength();
                break;
            case "months":
                endMonth = startMonth + Budget.getPeriodLength();
                break;
            case "years":
                endYear = startYear + Budget.getPeriodLength();
                break;
            default:
                break;
        }
        //Handle Month/Year carry over(s)
        while (endMonth > 12)
        {
            endMonth -= 12;
            endYear++;
            currentMonth = endMonth;
        }
        while (endDay > Utilities.getLastDayOfMonth(currentMonth))
        {
            endDay -= Utilities.getLastDayOfMonth(currentMonth);
            endMonth++;
            if (endMonth > 12)
            {
                endMonth -= 12;
                endYear++;
            }
            currentMonth = endMonth;
        }
        //Construct end date, and set as end of timeframe.
        LocalDate endTime = LocalDate.of(endYear, endMonth, endDay);
        Budget.setTimeframe(new Timeframe(startTime, endTime));

        Budget.setTitle(title);
        for (STATEMENT s : statements){
            s.evaluate();
        }
        displaymode.evaluate();
    }

    @Override
    public void namecheck() {
        //No namecheck() needed.
    }

    @Override
    public void typecheck() {
        // no typecheck needed
    }



}
