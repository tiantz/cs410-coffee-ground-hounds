package ast;

import exceptions.ParsingException;
import libs.Node;
import utils.Consts;

public class STATEMENT extends Node {

    STATEMENT statement;

    @Override
    public void parse() {
        if (tokenizer.checkToken(Consts.CURRENTBAL)) {
            statement = new BALANCE();
        } else if (tokenizer.checkToken(Consts.EARN)) {
            statement = new INCOME();
        } else if (tokenizer.checkToken(Consts.SAVE)) {
            statement = new SAVE();
        } else if (tokenizer.checkToken(Consts.SPEND)) {
            statement = new SPENDING();
        } else {
            throw new ParsingException("Unexpected token "+tokenizer.getNext());
        }
        statement.parse();
    }

    @Override
    public void evaluate() {
        statement.evaluate();
    }

    @Override
    public void namecheck() {
        statement.namecheck();
    }

    @Override
    public void typecheck() {
        statement.typecheck();
    }

}
