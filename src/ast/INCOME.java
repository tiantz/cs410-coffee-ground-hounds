package ast;

import libs.Node;
import libs.Budget;
import exceptions.TypeCheckException;
import utils.Consts;


public class INCOME extends STATEMENT {

    String amount;
    String category;
    PERIOD period;

    @Override
    public void parse() {
        // Assumes input looks like "Earn, 100, from, bus driving, every, 1 month"
        tokenizer.getAndCheckNext(Consts.EARN);
        amount = tokenizer.getNext(); // TYPECHECK: is integer

        tokenizer.getAndCheckNext(Consts.FROM);
        category = tokenizer.getNext();

        tokenizer.getAndCheckNext(Consts.EVERY);
        period = new PERIOD();
        period.parse();
    }

    @Override
    public void evaluate() {
        float amountData = Float.parseFloat(amount);
        int length = Integer.parseInt(period.length);
        Budget.makeIncome(category, amountData, length, period.type);
    }

    @Override
    public void namecheck() {
        Node.names.add(category);
    }

    @Override
    public void typecheck() {
        try {
            Integer.parseInt(amount);
        } catch (NumberFormatException n) {
            throw new TypeCheckException("Expected balance to be integer, got "+amount+" instead.");
        }
    }

}
