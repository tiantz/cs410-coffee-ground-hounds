package ast;

import libs.Budget;
import exceptions.TypeCheckException;
import libs.Node;
import utils.Consts;

public class SPENDING extends STATEMENT {

    String amount;
    String category;
    PERIOD period;

    @Override
    public void parse() {
        // Assume input looks like "Spend, 10, from, food, every, 1 day"
        tokenizer.getAndCheckNext(Consts.SPEND);
        amount = tokenizer.getNext(); // TYPECHECK: Check if integer

        tokenizer.getAndCheckNext(Consts.FROM);
        category = tokenizer.getNext();

        tokenizer.getAndCheckNext(Consts.EVERY);
        period = new PERIOD();
        period.parse();
    }

    @Override
    public void evaluate() {
        float amountData = Float.parseFloat(amount);
        int length = Integer.parseInt(period.length);
        Budget.makeSpending(category, amountData, length, period.type);
    }

    @Override
    public void namecheck() {
        Node.names.add(category);
    }

    @Override
    public void typecheck() {
        try {
            Integer int_amount = Integer.parseInt(amount);
        } catch (NumberFormatException n) {
            throw new TypeCheckException("Expected balance to be integer, got "+amount+" instead.");
        }
    }

}
