package ast;

import exceptions.EvaluationException;
import exceptions.NameCheckException;
import libs.Budget;
import libs.Node;
import utils.Consts;
import utils.Utilities;

import java.time.LocalDate;

public class SELECT extends Node {

    String category = "";
    TFRAME tframe;

    @Override
    public void parse() {
        // Assume input looks like "only show, food, from, Sep 1, to, Oct 1"

        tokenizer.getAndCheckNext(Consts.ONLYSHOW_NOSPACE);
        if ((!(tokenizer.checkToken(Consts.FROM)))
                && tokenizer.moreTokens()) {
            category = tokenizer.getNext(); // NAMECHECK: check category has already been established
        }

        if (tokenizer.checkToken(Consts.FROM)) {
            tframe = new TFRAME();
            tframe.parse();
        }
    }

    @Override
    public void evaluate() {
        LocalDate startDate;
        LocalDate endDate;
        if (tframe != null)
        {
            //If timeframe was specified...
            int startYear = LocalDate.now().getYear();
            int startMonth = Utilities.monthToInt(tframe.from.month);
            int startDay = Integer.parseInt(tframe.from.day);
            int endMonth = Utilities.monthToInt(tframe.to.month);
            int endDay = Integer.parseInt(tframe.to.day);
            int endYear = Utilities.assumeSuitableYear(endMonth, endDay);
            startDate = LocalDate.of(startYear, startMonth, startDay);
            endDate = LocalDate.of(endYear, endMonth, endDay);
            LocalDate budgetEndDate = Budget.getTimeframe().getToDate();
            if (endDate.isAfter(budgetEndDate))
            {
                throw new EvaluationException("End date for select statement: " + endDate +" is later than the budget's end date." + budgetEndDate);
            }
        }
        else
        {
            //If timeframe was not specified...
            startDate = Budget.getTimeframe().getFromDate();
            endDate = Budget.getTimeframe().getToDate();
        }

        Budget.makeSelect(category, startDate, endDate);
    }

    @Override
    public void namecheck() {
        if (!Node.names.contains(category)){
            throw new NameCheckException(category);
        }
    }

    @Override
    public void typecheck() {
        // Todo
    }

}
