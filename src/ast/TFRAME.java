package ast;

import libs.Node;
import utils.Consts;

public class TFRAME extends Node {

    DATE from;
    DATE to;

    @Override
    public void parse() {
        // Assumes the input is something like "from, Sep 1, to, Nov 1"
        tokenizer.getAndCheckNext(Consts.FROM);
        from = new DATE();
        from.parse();

        tokenizer.getAndCheckNext(Consts.TO);
        to = new DATE();
        to.parse();
    }

    @Override
    public void evaluate() {
        //Evaluation Unused
    }

    @Override
    public void namecheck() {
        //No namecheck() needed.
    }

    @Override
    public void typecheck() {
        // no typechecks needed
    }

}
