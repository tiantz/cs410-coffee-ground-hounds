package ast;

import exceptions.EvaluationException;
import exceptions.TypeCheckException;
import libs.Budget;
import libs.Saving;
import utils.Consts;
import utils.Utilities;

import java.time.LocalDate;

public class SAVE extends STATEMENT {

    DATE saveby;
    String amount;
    String purpose;

    @Override
    public void parse() {
        // Assumes the input looks like "Save amount by Nov 1"
        tokenizer.getAndCheckNext(Consts.SAVE);
        amount = tokenizer.getNext();  // TYPECHECK: is integer?

        tokenizer.getAndCheckNext(Consts.BY);
        saveby = new DATE();  // EVALUATION: is this date before the end of the period?
        saveby.parse();

        tokenizer.getAndCheckNext(Consts.FOR_NOSPACE);
        purpose = tokenizer.getNext();
    }

    @Override
    public void evaluate() {
        Saving saving = new Saving();
        saving.setAmount(Integer.parseInt(amount));
        saving.setPurpose(purpose);
        // TODO: Store purpose string in namecheck() (?)
        int month = Utilities.monthToInt(saveby.month);
        int day = Integer.parseInt(saveby.day);
        int year = Utilities.assumeSuitableYear(month, day);

        LocalDate deadline = LocalDate.of(year, month, day);
        LocalDate budgetEndDate = Budget.getTimeframe().getToDate();
        if (deadline.isAfter(budgetEndDate))
        {
            throw new EvaluationException("Deadline for Save statement: " + deadline +" is later than the budget's end date." + budgetEndDate);
        }

        saving.setDeadline(deadline);
        Budget.addSaving(saving);
    }

    @Override
    public void namecheck() {
        // Todo (?)
    }

    @Override
    public void typecheck() {
        try {
            Integer.parseInt(amount);
        } catch (NumberFormatException n) {
            throw new TypeCheckException("Expected balance to be integer, got "+amount+" instead.");
        }
    }

}
