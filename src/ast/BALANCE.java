package ast;

import exceptions.TypeCheckException;
import utils.Consts;
import libs.Budget;

public class BALANCE extends STATEMENT {

    String balance;

    @Override
    public void parse() {
        // Assumes input looks like "Current balance is 5"
        tokenizer.getAndCheckNext(Consts.CURRENTBAL);
        balance = tokenizer.getNext(); // TYPECHECK: check is integer
    }

    @Override
    public void evaluate() {
        Budget.setBalance(Integer.parseInt(balance));
    }

    @Override
    public void namecheck() {
        //No namecheck() needed.
    }

    @Override
    public void typecheck() {
        try {
            Integer.parseInt(balance);
        } catch (NumberFormatException n) {
            throw new TypeCheckException("Expected balance to be integer, got "+balance+" instead.");
        }
    }

}
