package ast;

import exceptions.ParsingException;
import exceptions.TypeCheckException;
import libs.Node;
import utils.Consts;
import utils.Utilities;

import java.util.regex.PatternSyntaxException;

public class DATE extends Node {

    String month;
    String day;

    @Override
    public void parse() {
        // Assume format like "Oct 1"
        String raw_date = tokenizer.getNext();
        try {
            String[] date = raw_date.split(" ");
            month = date[0];  // TYPECHECK: valid month
            day = date[1];  // TYPECHECK: valid day given month
        } catch (PatternSyntaxException e) {
            throw new ParsingException("Expected space in "+raw_date);
        }
    }

    @Override
    public void evaluate() {
        //Evaluation unused
    }

    @Override
    public void namecheck() {
        //No namecheck() needed.
    }

    @Override
    public void typecheck() {
        boolean is_valid_month = Consts.PERIODTYPES.contains(month);

        try {
            Integer.parseInt(day);
        } catch (NumberFormatException n) {
            throw new TypeCheckException("Expected balance to be integer, got "+day+" instead.");
        }

        boolean is_valid_day = Integer.parseInt(day) <= Utilities.getLastDayOfMonth(Utilities.monthToInt(month));  // maybe td? check days based on month

        if (!is_valid_month) {
            throw new TypeCheckException(month+" is not a valid month.");
        }

        if (!is_valid_day) {
            throw new TypeCheckException(day+" is not a valid day.");
        }
    }

}
