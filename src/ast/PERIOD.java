package ast;

import exceptions.ParsingException;
import exceptions.TypeCheckException;
import libs.Node;
import utils.Consts;

import java.util.regex.PatternSyntaxException;

public class PERIOD extends Node {

    String length;
    String type;

    @Override
    public void parse() {
        // Assumes the input is something like "3 month"
        String raw_period = tokenizer.getNext();
        try {
            String[] split_period = raw_period.split(" ");
            length = split_period[0];  // TYPECHECK: Check if integer
            type = split_period[1];  // TYPECHECK: Check if valid period using Const.PERIODTYPES. use Contains for plurals!
        } catch (PatternSyntaxException e) {
            throw new ParsingException("Expected space in "+raw_period);
        }
    }

    @Override
    public void evaluate() {
        //Evaluation unused
    }

    @Override
    public void namecheck() {
        //No namecheck() needed.
    }

    @Override
    public void typecheck() {
        try {
            Integer.parseInt(length);
        } catch (NumberFormatException n) {
            throw new TypeCheckException("Expected balance to be integer, got "+length+" instead.");
        }
        boolean validType = Consts.PERIODTYPES.contains(type);

        if (!validType) {
            throw new TypeCheckException(type+" is not a valid period type.");
        }

    }
}
