package ast;

import exceptions.TypeCheckException;
import libs.Budget;
import libs.Node;
import utils.Consts;

public class DISPLAYMODE extends Node {

    SELECT select;

    String displaymode;

    @Override
    public void parse() {
        // Assuming input is something like "Display as, table"
        tokenizer.getAndCheckNext(Consts.DISPLAYAS);
        displaymode = tokenizer.getNext();  // TYPECHECK: check type is valid displaymode

        // "only show ..."
        if (tokenizer.checkToken(Consts.ONLYSHOW_NOSPACE)) {
            select = new SELECT();
            select.parse();
        }
    }

    @Override
    public void evaluate() {
        Budget.DisplayMode mode = Budget.DisplayMode.TABLE;
        if (displaymode != null) {
            if (displaymode.equals("bar"))
            {
                mode = Budget.DisplayMode.BAR;
            }
            else if (displaymode.equals("table"))
            {
                mode = Budget.DisplayMode.TABLE;
            }

            else if (displaymode.equals("pie"))
            {
                mode = Budget.DisplayMode.PIE;
            }
        }
        Budget.setDisplayMode(mode);
        //Leave Budget.selectInfo as null if it's not specified.
        if (select != null)
        {
            select.evaluate(); //Otherwise, create it.
        }

    }

    @Override
    public void namecheck() {
        //No namecheck() needed.
    }

    @Override
    public void typecheck() {
        if (!Consts.DISPLAYTYPES.contains(displaymode)) {
            throw new TypeCheckException(displaymode +"is not a valid display mode.");
        }
    }

}
