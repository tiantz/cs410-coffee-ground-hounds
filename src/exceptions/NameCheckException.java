package exceptions;

public class NameCheckException extends RuntimeException {
    public NameCheckException(String msg) {
        super(msg);
    }
}
