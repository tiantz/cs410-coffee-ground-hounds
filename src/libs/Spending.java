package libs;

public class Spending extends Expenditure
{
    public Spending()
    {

    }

    public Spending(String category, float amount, int periodLength, String periodType)
    {
        super(category, amount, periodLength, periodType);
    }
}