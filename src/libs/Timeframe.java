package libs;

import java.time.temporal.ChronoUnit;
import java.time.LocalDate;

public class Timeframe
{
    private LocalDate from;
    private LocalDate to;

    public Timeframe(LocalDate from, LocalDate to)
    {
        this.from = from;
        this.to = to;
    }

    public LocalDate getFromDate()
    {
        return from;
    }

    public LocalDate getToDate()
    {
        return to;
    }

    public void setFrom(LocalDate from)
    {
        this.from = from;
    }

    public void setTo(LocalDate to)
    {
        this.to = to;
    }

    public Integer getDays() {
        return ((int) ChronoUnit.DAYS.between(from, to));
    }
}