package libs;

import utils.Consts;

public abstract class Expenditure
{
    private float amount;
    private String category;
    private int periodLength;
    private String periodType;

    public Expenditure()
    {
        category = "";
        amount = 0.0f;
        periodLength = 0;
        periodType = "";
    }

    public Expenditure(String category, float amount, int periodLength, String periodType)
    {
        this.category = category;
        this.amount = amount;
        this.periodLength = periodLength;
        this.periodType = periodType;
    }

    public float getAmount()
    {
        return amount;
    }

    public float getAmountTotal(int totalPeriodLength, String totalPeriodType)
    {
        Integer factor = 1;
        if (periodType.contains(Consts.DAY)) {
            if (totalPeriodType.contains(Consts.DAY)) {
                factor = Integer.max(totalPeriodLength, factor);
            } else if (totalPeriodType.contains(Consts.WEEK)) {
                factor = totalPeriodLength*7;
            } else if (totalPeriodType.contains(Consts.MONTH)) {
                factor = totalPeriodLength*30;
            } else if (totalPeriodType.contains(Consts.YEAR)) {
                factor = totalPeriodLength*365;
            }
        } else if (periodType.contains(Consts.WEEK)) {
            if (totalPeriodType.contains(Consts.WEEK)) {
                factor = Integer.max(totalPeriodLength, factor);
            } else if (totalPeriodType.contains(Consts.MONTH)) {
                factor = totalPeriodLength*4;
            } else if (totalPeriodType.contains(Consts.YEAR)) {
                factor = totalPeriodLength*52;
            }
        } else if (periodType.contains(Consts.MONTH)) {
            if (totalPeriodType.contains(Consts.MONTH)) {
                factor = Integer.max(totalPeriodLength, factor);
            } else if (totalPeriodType.contains(Consts.YEAR)) {
                factor = totalPeriodLength*12;
            }
        } else if (periodType.contains(Consts.YEAR)) {
            if (totalPeriodType.contains(Consts.YEAR)) {
                factor = Integer.max(totalPeriodLength, factor);
            }
        }
        return amount*factor;
    }

    public String getCategory()
    {
        return category;
    }

    public int getPeriodLength() {
        return periodLength;
    }

    public String getPeriodType()
    {
        return periodType;
    }

    public void setAmount(float amount)
    {
        this.amount = amount;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public void setPeriodLength(int periodLength) {
        this.periodLength = periodLength;
    }

    public void setPeriodType(String periodType)
    {
        this.periodType = periodType;
    }

}