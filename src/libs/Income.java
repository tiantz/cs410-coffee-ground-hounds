package libs;

public class Income extends Expenditure
{
    public Income()
    {

    }

    public Income(String category, float amount, int periodLength, String periodType)
    {
        super(category, amount, periodLength, periodType);
    }
}