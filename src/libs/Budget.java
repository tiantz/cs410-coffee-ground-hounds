 package libs;

 import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

 public class Budget
{
    private static Budget budget;

    //TODO: Maybe move this to its own file.
    public enum DisplayMode
    {
        TABLE,
        PIE,
        BAR
    }

    private static String title;

    private static int periodLength;
    private static String periodType;

    private static Timeframe tFrame;
    private static float balance;
    private static List<Income> incomes = new ArrayList();
    private static List<Spending> spendings = new ArrayList();
    private static List<Saving> savings = new ArrayList();
    //    enum of displaymode
    private static DisplayMode displayMode;



    private static Select selectInfo;

    private Budget()
    {
        title = "DEFAULT TITLE";
        balance = (float) 0.0;
        displayMode = DisplayMode.TABLE;
        periodLength = 0;
        periodType = "";
    }

    public static Budget getInstance()
    {
        if (budget == null)
            budget = new Budget();
            refreshBudget();

        return budget;
    }

    public static void refreshBudget() {
        budget = new Budget();
        title = "";

        periodLength = 0;
        periodType = "";

        tFrame = null;
        balance = 0;
        incomes = new ArrayList();
        spendings = new ArrayList();
        savings = new ArrayList();
        displayMode = null;

    }

    public static String getTitle()
    {
        return title;
    }

    public static float getBalance()
    {
        return balance;
    }

    public static Timeframe getTimeframe() {
        return tFrame;
    }

    public static int getPeriodLength() {
        return periodLength;
    }

    public static String getPeriodType() {
        return periodType;
    }

    public static List<Income> getIncomes()
    {
        return incomes;
    }

    public static List<Spending> getSpendings()
    {
        return spendings;
    }

    public static List<Saving> getSavings()
    {
        return savings;
    }

    public static DisplayMode getDisplayMode()
    {
        return displayMode;
    }

    public static Select getSelectInfo() {
        return selectInfo;
    }

    public static void setTitle(String titleData)
    {
        title = titleData;
    }

    public static void setBalance(float balanceData)
    {
        balance = balanceData;
    }

    public static void setTimeframe(Timeframe tFrame) {
        Budget.tFrame = tFrame;
    }

    public static void setPeriodLength(int periodLength) {
        Budget.periodLength = periodLength;
    }

    public static void setPeriodType(String periodType) {
        Budget.periodType = periodType;
    }

    public static void addIncome(Income income)
    {
        incomes.add(income);
    }

    public static void makeIncome(String category, float amount, int periodLength, String periodType)
    {
        Income income = new Income(category, amount, periodLength, periodType);
        addIncome(income);
    }

    public static void addSpending(Spending spending)
    {
        spendings.add(spending);
    }

    public static void makeSpending(String category, float amount, int periodLength, String periodType)
    {
        Spending spending = new Spending(category, amount, periodLength, periodType);
        addSpending(spending);
    }

    public static void addSaving(Saving saving)
    {
        savings.add(saving);
    }

    public static void setDisplayMode(DisplayMode mode)
    {
        displayMode = mode;
    }

    public static void setSelectInfo(Select selectInfo) {
        Budget.selectInfo = selectInfo;
    }

    public static void makeSelect(String category, LocalDate from, LocalDate to)
    {
        Select select = new Select();
        select.setCategory(category);
        Timeframe tf = new Timeframe(from, to);
        select.setTimeframe(tf);
        setSelectInfo(select);
    }


}