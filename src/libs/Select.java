package libs;

import java.time.LocalDate;

public class Select
{
    private String category;
    private Timeframe tFrame;

    public Select()
    {
        category = "";
        LocalDate d = LocalDate.now();
        tFrame = new Timeframe(d, d);
    }

    public String getCategory()
    {
        return category;
    }

    public Timeframe getTimeframe()
    {
        return tFrame;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public void setTimeframe(Timeframe tFrame)
    {
        this.tFrame = tFrame;
    }

}