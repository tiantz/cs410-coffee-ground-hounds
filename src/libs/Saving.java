package libs;

import java.time.LocalDate;

public class Saving
{
    //TODO: Maybe split this into two subclasses?
    private float amount;
    private LocalDate deadline;
    private String purpose;

    public Saving()
    {
        amount = 0.0f;
        deadline = LocalDate.now();
        purpose = "";
    }

    public Saving(float amount, LocalDate deadline, String purpose)
    {
        this.amount = amount;
        this.deadline = deadline;
        this.purpose = purpose;
    }

    public float getAmount()
    {
        return amount;
    }

    public String getPurpose()
    {
        return purpose;
    }

    public LocalDate getDeadline()
    {
        return deadline;
    }

    public void setAmount(float amount)
    {
        this.amount = amount;
    }

    public void setPurpose(String purpose)
    {
        this.purpose = purpose;
    }

    public void setDeadline(LocalDate deadline)
    {
        this.deadline = deadline;
    }
}