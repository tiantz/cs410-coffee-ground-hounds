# cs410-coffee-ground-hounds
## 
## Grammar
* __PROGRAM__ ::= "Make a budget for next " INTEGER __PERIOD__ __TFRAME__ ["Called" [__TITLE__]]? __STATEMENT__*  __DISPLAYMODE__+
  * __PERIOD__ ::= “month” | “year” | “week”
  * __TITLE__ ::= STRING
* __TFRAME__ ::= ["from" __DATE__ "to" __DATE__]?
  * __DATE__ ::= __MONTH__ [1-31]
      * __MONTH__ ::= "Jan"|"Feb"|"Mar"|...|"Dec"
* __STATEMENT__ ::= __BALANCE__ | __INCOME__ | __SAVE__ | __GOAL__ | __SPENDING__
* __BALANCE__ ::= "Current balance is" INTEGER
* __INCOME__ ::= "Earn" INTEGER from __CATEGORY__ [every __PERIOD__]?
  * __CATEGORY__ ::= STRING
* __SAVE__ ::= "Save" INTEGER ["%"]? "by" __DATE__ ["for" __PURPOSE__]?
  * __PURPOSE__ ::= STRING
* __SPENDING__ ::= "Spend" INTEGER [%]? "on" __CATEGORY__ ["every" __PERIOD__]?
* __DISPLAYMODE__ ::= "Display as" __MODE__ __SELECT__?
  * __MODE__ ::= “table” | “pie” | “bar”
* __SELECT__ ::= "Only show" __SELECTTYPE__
  * __SELECTTYPE__ ::= __SELCAT__|__SELTIME__|__SELBOTH__
      * __SELECTCAT__ ::= __CATEGORY__
      * __SELECTTIME__ ::= "from" __TFRAME__
      * __SELECTBOTH__ ::= __CATEGORY__ "from" __TFRAME__